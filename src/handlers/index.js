const home = require('./home');
const example = require('./example');

module.exports = {
  home,
  example
};
